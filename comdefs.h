// @timcdoc started ca 2014                                                        **/
#include <unistd.h>
#include <stdio.h>
#include <netdb.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <sched.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/prctl.h>
#include <sys/sysmacros.h>
#ifdef ONION
#define LOCAL_BUFFER_SIZE 8192
#else
#define LOCAL_BUFFER_SIZE 32768
#endif

#ifdef DEBUG
#define DEBUGPRINTF(a) printf a ;fflush(stdout);
#else
#define DEBUGPRINTF(a) 
#endif

#define CONTEXT_SIZE 16
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define SEEK_END 2
#define SEEK_SET 0
#define NROUTERTHREADS 5
#define UNINIT_STATE 2
#define READY_STATE 1
#define BUSY_STATE 0
#ifdef ONION
#define GURL_BASE_LEN 4
#define GURL_BASE_NAME "/www"
// This ud.js is a magic name, this needs to be handled differently, when I have the time.
#define GURL_GENERATED_NAME "/www/ud.js"
#else
#define GURL_BASE_LEN 13
#define GURL_BASE_NAME "/var/www/html"
// This ud.js is a magic name, this needs to be handled differently, when I have the time.
#define GURL_GENERATED_NAME "/var/www/html/ud.js"
#endif

#ifndef TRUE
#define TRUE 1
#endif
#ifndef FALSE
#define FALSE 0
#endif

typedef uint32_t DWORD;
typedef unsigned char BOOL;

typedef struct _ext_thread_glob {
    pthread_t td;
    sem_t go_sem;
    struct sockaddr_in cli_addr;
    char *nctx_port;
    char *nctx_name;
    int    ext_sock;
    char state;
} EXT_THREAD_GLOB, *PEXT_THREAD_GLOB;

extern unsigned char gurl_base[GURL_BASE_LEN]; 

extern EXT_THREAD_GLOB ext_at_server[NROUTERTHREADS];
extern char abuffer[NROUTERTHREADS][LOCAL_BUFFER_SIZE+1];
extern struct sockaddr_in ext_sa;
extern int    volatile ext_sock;
extern int    volatile gnotexit;
extern pthread_mutex_t tcpip_mutex;
extern unsigned char volatile gd1, gd2, gd3, gd4, gd5, gd6, gcolon;
extern unsigned char volatile gname, gvalue;
extern char *ghostpsz;

extern char *strrev( char *psz );
extern int html_len( char *psz );
extern void init_threads( void );
extern void *serverthread( void *new_sock );
extern void ultoa( long val, int base, char *psz );
extern int server_embed( char *hostname, int iext_port );

#define CRITICAL_SECTION_ENTER(x) pthread_mutex_lock( &x )
#define CRITICAL_SECTION_LEAVE(x) pthread_mutex_unlock( &x )

#define SZE(a) (sizeof(a)/sizeof(a[0]))
#define MAX(a,b) (((a)>(b))?(a):(b))
