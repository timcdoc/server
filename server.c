/*************************************************************************************/
/**                                                                                 **/
/** server.c contains context sequence web server server                            **/
/** @timcdoc started ca 2014                                                        **/
/**                                                                                 **/
/*************************************************************************************/

#include "comdefs.h"

unsigned char volatile gd1, gd2, gd3, gd4, gd5, gd6, gcolon; //not used here, just for stand alone build, better ways.
unsigned char volatile gname, gvalue;
char *ghostpsz;

int main( int argc, char *argv[] )

{
    if ( argc != 4 )
        {
        printf( "usage:%s hostname port directory\neg: %s 192.168.0.1 80 /var/www/html\n", argv[0], argv[0] );
        }
    else
        {
        ghostpsz = strdup( argv[3] );
        server_embed( argv[1], strtoul( argv[2], NULL, 10 ) );
        pthread_join( ext_at_server[0].td, NULL );
        free( ghostpsz );
        }
    return( 0 );
}

