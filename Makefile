#Note this is half of the threads used in router, the other half are available for a cloud
MAXTHREADS=$(shell bash -c "(grep -q tinycore /proc/version && echo 8) || (grep -q \"\-v71+ \" /proc/version && echo 8) || echo 32")
CFLAGS=$(shell bash -c "(grep -q x86_64 /proc/version && echo \"-march=x86-64\" ) || echo -e")
CC=gcc
LD=gcc
LDFLAGS=-lm -ldl -lpthread
#CFLAGS=-march=x86-64 -g -ggdb -lpthread -pthread -rdynamic -DMAXTHREADS=$(MAXTHREADS)
CFLAGS+=-lpthread -pthread -rdynamic -DMAXTHREADS=$(MAXTHREADS)

RM=/bin/rm -f

OBJS=server.o server_embed.o init_threads.o serverthread.o support.o

PROG=server

SRCS=server.c server_embed.c init_threads.c serverthread.c support.c

all: $(PROG) tinyserver.h

$(PROG): $(OBJS) $(USRLIBS)
	$(LD) $(LDFLAGS) -o $(PROG) $(OBJS) $(USRLIBS) -lpthread
	yes | cp -f $(PROG) /usr/bin/$(PROG) || yes | sudo cp -f $(PROG) /usr/bin/$(PROG) || cp -f $(PROG) ~/.local/bin/

tinyserver.h: inittab.awk
	awk -f inittab.awk >tinyserver.h

%.o: %.c tinyserver.h
	$(CC) $(CFLAGS) -c $< -lpthread

clean:
	$(RM) $(OBJS)

