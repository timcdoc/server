# @timcdoc written ca 2014
# This generates all the states for a full web server, the necessity of adding 
# specific TLA extensions is deliberate.

BEGIN {
#Add new extensions as States here.
    TEXTPLAIN_STATE=0
    AUDIOMID_STATE=1
    AUDIOMPG_STATE=2
    AUDIOWMA_STATE=3
    IMAGEBMP_STATE=4
    IMAGEGIF_STATE=5
    IMAGEICO_STATE=6
    IMAGEJPG_STATE=7
    IMAGEPNG_STATE=8
    IMAGESVG_STATE=9
    PDF_STATE=10
    TEXTCSS_STATE=11
    TEXTHTML_STATE=12
    VIDEOMPG_STATE=13
    VIDEOWMV_STATE=14
    ERROR_STATE=VIDEOWMV_STATE+1
    START_STATE=ERROR_STATE+1

#Convention 0th element of arrays is the size of the array.
    ARRAY_SIZE=0
#Make sure to match up any new types(states) with the new extension.
    atype[1] = TEXTHTML_STATE;aext[1] = "htm";atypetext[TEXTHTML_STATE]="text/html"
    atype[2] = TEXTHTML_STATE;aext[2] = "html";
    atype[3] = TEXTPLAIN_STATE;aext[3] = "lsp";atypetext[TEXTPLAIN_STATE]="text/plain"
    atype[4] = IMAGEBMP_STATE;aext[4] = "bmp";atypetext[IMAGEBMP_STATE]="image/bmp"
    atype[5] = IMAGEGIF_STATE;aext[5] = "gif";atypetext[IMAGEGIF_STATE]="image/gif"
    atype[6] = IMAGEPNG_STATE;aext[6] = "png";atypetext[IMAGEPNG_STATE]="image/png"
    atype[7] = IMAGEJPG_STATE;aext[7] = "jpg";atypetext[IMAGEJPG_STATE]="image/jpeg"
    atype[8] = IMAGEJPG_STATE;aext[8] = "jpeg";
    atype[9] = TEXTHTML_STATE;aext[9] = "shtml";
    atype[10] = IMAGEICO_STATE;aext[10] = "ico";atypetext[IMAGEICO_STATE]="image/vnd.microsoft.ico"
    atype[11] = VIDEOWMV_STATE;aext[11] = "wmv";atypetext[VIDEOWMV_STATE]="video/x-ms-wmv"
    atype[12] = AUDIOMID_STATE;aext[12] = "mid";atypetext[AUDIOMID_STATE]="audio/midi"
    atype[13] = PDF_STATE;aext[13] = "pdf";atypetext[PDF_STATE]="application/pdf"
    atype[14] = IMAGESVG_STATE;aext[14] = "svg";atypetext[IMAGESVG_STATE]="image/svg+xml"
    atype[15] = TEXTCSS_STATE;aext[15] = "css";atypetext[TEXTCSS_STATE]="text/css"
    atype[16] = AUDIOWMA_STATE;aext[16] = "wma";atypetext[AUDIOWMA_STATE]="audio/x-ms-wma"
    atype[17] = VIDEOMPG_STATE;aext[17] = "mpg";atypetext[17]="video/mpeg"
    atype[18] = AUDIOMPG_STATE;aext[18] = "mp3";atypetext[18]="audio/mpeg"
#Other ways to do this, just make sure ARRAY_SIZE is correct.
    aext[ARRAY_SIZE]=18

    START_HEAD_STATE = START_STATE+1
    START_GET_STATE = START_HEAD_STATE+1
    START_POST_STATE = START_GET_STATE+1
    cstate=START_POST_STATE+1

#Continuing to set up for generated states based on 
    amethod[START_HEAD_STATE] = "HEAD "
    amethod[START_GET_STATE] = "GET "
    amethod[START_POST_STATE] = "POST "

    MAXCHAR=256

# chstate_ch_to_state contains the state of chars so far then indicies on current state and next char,
# value returned is new state, this is automated after initial starting points.
    for ( imethod = START_HEAD_STATE; imethod <= START_POST_STATE; imethod++ ) {
        istate = START_STATE
        len = length(amethod[imethod])
        for ( ich = 1; ich <= len; ich++ ) {
            ch = substr( amethod[imethod], ich, 1 )
            if ( (istate, ch) in chstate_ch_to_state ) {
                istate = chstate_ch_to_state[istate,ch]
            } else {
                chstate_ch_to_state[istate,ch] = cstate
#                printf( "chstate_ch_to_state[%d,\"%s\"] = %d\n", istate, ch, cstate )
                istate = cstate
                cstate++
            }
        }
        printf( "#define %*.*s_STATE %d\n", len-1, len-1, amethod[imethod], istate )
        if ( imethod == START_HEAD_STATE ) {
            action_state[istate] = "HEAD_STATE"
            HEAD_STATE = istate
        } else if ( imethod == START_GET_STATE ) {
            GET_STATE = istate
            action_state[istate] = "GET_STATE"
        } else if ( imethod == START_POST_STATE ) {
            POST_STATE = istate
            action_state[istate] = "POST_STATE"
        }
    }

    SLASH_STATE = cstate++
    FILENAME_STATE = cstate++
    FIN_STATE = cstate++
    for ( i in action_state ) {
        chstate_ch_to_state[i,"/"] = SLASH_STATE
        chstate_ch_to_state[SLASH_STATE,"/"] = ERROR_STATE
    }

    for ( i = 1; i <= 26; i++ ) {
        alphanumerics[sprintf("%c", 64+i )] = 1
        alphanumerics[sprintf("%c", 32+64+i )] = 1
    }
    for ( i = 0; i <= 9; i++ ) {
        alphanumerics[sprintf("%c", 48+i )] = 1
    }
    alphanumerics[":"] = 1
    alphanumerics["_"] = 1

    QUEST_STATE = cstate++
    LVAR_STATE = cstate++
    RVAR_STATE = cstate++
    EQUAL_STATE = cstate++
    AMPER_STATE = cstate++
    START_EXTNAME_STATE = cstate++

    for ( i in alphanumerics ) {
        chstate_ch_to_state[SLASH_STATE,i] = FILENAME_STATE
        chstate_ch_to_state[FILENAME_STATE,i] = FILENAME_STATE
        chstate_ch_to_state[QUEST_STATE,i] = LVAR_STATE
        chstate_ch_to_state[AMPER_STATE,i] = LVAR_STATE
        chstate_ch_to_state[LVAR_STATE,i] = LVAR_STATE
        chstate_ch_to_state[EQUAL_STATE,i] = RVAR_STATE
        chstate_ch_to_state[RVAR_STATE,i] = RVAR_STATE
    }
    chstate_ch_to_state[LVAR_STATE,"="] = EQUAL_STATE
    chstate_ch_to_state[RVAR_STATE,"&"] = AMPER_STATE
    chstate_ch_to_state[QUEST_STATE," "] = FIN_STATE
    chstate_ch_to_state[AMPER_STATE," "] = FIN_STATE

    chstate_ch_to_state[SLASH_STATE,"."] = START_EXTNAME_STATE
    chstate_ch_to_state[START_EXTNAME_STATE,"."] = START_EXTNAME_STATE
    chstate_ch_to_state[FILENAME_STATE,"."] = START_EXTNAME_STATE
    chstate_ch_to_state[SLASH_STATE,"?"] = QUEST_STATE
    chstate_ch_to_state[FILENAME_STATE,"?"] = QUEST_STATE
    chstate_ch_to_state[FILENAME_STATE,"/"] = SLASH_STATE
    chstate_ch_to_state[EQUAL_STATE," "] = FIN_STATE
    chstate_ch_to_state[AMPER_STATE," "] = FIN_STATE
    chstate_ch_to_state[FILENAME_STATE," "] = FIN_STATE
    chstate_ch_to_state[RVAR_STATE," "] = FIN_STATE
    chstate_ch_to_state[SLASH_STATE," "] = FIN_STATE

    first_ext_state = cstate
    for ( iext = 1; iext <= aext[ARRAY_SIZE]; iext++ ) {
        istate = START_EXTNAME_STATE
        for ( ich = 1; ich <= length(aext[iext]); ich++ ) {
            ch = substr( aext[iext], ich, 1 )
            if ( (istate, ch) in chstate_ch_to_state ) {
                istate = chstate_ch_to_state[istate,ch]
            } else {
                chstate_ch_to_state[istate,tolower(ch)] = cstate
                chstate_ch_to_state[istate,toupper(ch)] = cstate
                istate = cstate
                cstate++
            }
        }
        chstate_ch_to_state[istate,0] = atype[iext]
        chstate_ch_to_state[istate,"?"] = QUEST_STATE
        chstate_ch_to_state[istate,"."] = START_EXTNAME_STATE
        chstate_ch_to_state[istate,"/"] = SLASH_STATE
        chstate_ch_to_state[istate," "] = FIN_STATE
    }

    for ( iext = 1; iext <= aext[ARRAY_SIZE]; iext++ ) {
        istate = START_EXTNAME_STATE
        for ( ich = 1; ich <= length(aext[iext]); ich++ ) {
            ch = substr( aext[iext], ich, 1 )
            if ( ( ich < length(aext[iext]) ) && !( (istate,0) in chstate_ch_to_state ) ) {
                chstate_ch_to_state[istate,0] = TEXTPLAIN_STATE
            }
            for ( i in alphanumerics ) {
                if ( i != ch ) {
                    if ( (istate, i) in chstate_ch_to_state ) {
                    } else {
                        chstate_ch_to_state[istate,tolower(i)] = FILENAME_STATE
                        chstate_ch_to_state[istate,toupper(i)] = FILENAME_STATE
                    }
                }
            }
            chstate_ch_to_state[istate," "] = FIN_STATE # .c etc
            istate = chstate_ch_to_state[istate,ch]
        }
    }

    printf( "#define TEXTPLAIN_STATE %d\n", TEXTPLAIN_STATE )
    printf( "#define TEXTHTML_STATE %d\n", TEXTHTML_STATE )
    printf( "#define TEXTCSS_STATE %d\n", TEXTCSS_STATE )
    printf( "#define IMAGEJPG_STATE %d\n", IMAGEJPG_STATE )
    printf( "#define PDF_STATE %d\n", PDF_STATE )
    printf( "#define AUDIOMID_STATE %d\n", AUDIOMID_STATE )
    printf( "#define AUDIOWMA_STATE %d\n", AUDIOWMA_STATE )
    printf( "#define VIDEOWMA_STATE %d\n", VIDEOWMA_STATE )
    printf( "#define VIDEOMPG_STATE %d\n", VIDEOMPG_STATE )
    printf( "#define ERROR_STATE %d\n", ERROR_STATE )
    printf( "#define START_STATE %d\n", START_STATE )
    printf( "#define QUEST_STATE %d\n", QUEST_STATE )
    printf( "#define FIN_STATE %d\n", FIN_STATE )
    printf( "#define RVAR_STATE %d\n", RVAR_STATE )
    printf( "#define LVAR_STATE %d\n", LVAR_STATE )
    printf( "#define SLASH_STATE %d\n", SLASH_STATE )
    printf( "#define FILENAME_STATE %d\n", FILENAME_STATE )
    printf( "#define AMPER_STATE %d\n", AMPER_STATE )

    WEBSTATE_SIZE = cstate
    if ( WEBSTATE_SIZE > 255 ) {
        printf( "inittab.awk:PANIC:WEBSTATE_SIZE is %d\n", WEBSTATE_SIZE )
    }
    printf( "#define WEBSTATE_SIZE %d\n", WEBSTATE_SIZE )

    action_state[QUEST_STATE]="QUEST_STATE"
    action_state[FIN_STATE]="FIN_STATE"
    action_state[START_EXTNAME_STATE]="START_EXTNAME_STATE"
    action_state[ERROR_STATE]="ERROR_STATE"

#Make sure hanging states are set to error state. 
    for ( j = 0; j < WEBSTATE_SIZE; j++ ) {
        if ( (j,0) in chstate_ch_to_state ) {
        } else {
        chstate_ch_to_state[j,0] = ERROR_STATE
        }
        for ( i = 1; i < MAXCHAR; i++ ) {
           ch = sprintf("%c",i)
           if ( (j,ch) in chstate_ch_to_state ) {
           } else {
               chstate_ch_to_state[j,ch] = ERROR_STATE
           }
        }
    }
   
    printf( "//Autogenerated from awk -f inittab.awk, make sure that file is in sync with this one.\n" )
    printf( "unsigned char webstatetable[%d][%d] = {\n", WEBSTATE_SIZE, MAXCHAR )
    for ( j = 0; j < WEBSTATE_SIZE; j++ ) {
        printf( "{ %d, ", chstate_ch_to_state[j,0] )
        for ( i = 1; i < MAXCHAR; i++ ) {
           printf( "%d, ", chstate_ch_to_state[j,sprintf("%c",i)] )
           if ( (i % 32) == 0 ) {
               printf( "\n" )
           }
        }
        printf( "},\n" )
    }
    printf( "};\n" )
    printf( "unsigned char action_state[%d] = {\n", WEBSTATE_SIZE, MAXCHAR )
    for ( i = 0; i < WEBSTATE_SIZE; i++ ) {
           if ( i in action_state ) {
               printf( "1, " )
           } else {
               printf( "0, " )
           }
           if ( ((i+1) % 32) == 0 ) {
               printf( "\n" )
           }
    }
    printf( "};\n" )
    printf( "char *typetext[] = { \n" )
    for ( i = 0; i < ERROR_STATE; i++ ) {
        if ( i == (ERROR_STATE-1) ) {
            printf( "    \"%s\"\n", atypetext[i] )
        } else {
            printf( "    \"%s\",\n", atypetext[i] )
        }
    }
    printf( "};\n" )
}
