#!
#Assumption router has 8+8 threads
thisip=192.168.7.40
while (true)
do
    ps -o args >_server.tmp
    if grep -q "router 0 81" _server.tmp; then
        echo router already running.
    else
        echo starting router 0 81
        sudo router 0 81 2>>../router/router.err.log &
    fi
    
    if grep -q "lrouter 0 8001" _server.tmp; then
        echo lrouter already running.
    else
        echo starting lrouter 0 8001
        sudo lrouter 0 8001 2>>../lrouter/lrouter.err.log &
    fi
    
    for (( port=13016; port<13024; port++ ))
    do
        if grep -q "server $thisip $port" _server.tmp; then
            if ! [ -e /etc/router/81/GET,$thisip,$port ]; then
                touch /etc/router/81/GET,$thisip,$port
            fi
        else
            echo rm -f /etc/router/81/GET,$thisip,$port
            rm -f /etc/router/81/GET,$thisip,$port
            sleep 2
            echo touch /etc/router/81/GET,$thisip,$port
            touch /etc/router/81/GET,$thisip,$port
            echo starting server $thisip $port
            sudo server $thisip $port &
        fi
    done
    sleep 10
done
