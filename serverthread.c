/*************************************************************************************/
/**                                                                                 **/
/** serverthread.c contains the server threads.                                     **/
/** @timcdoc started ca 2014                                                        **/
/**                                                                                 **/
/*************************************************************************************/

#include <netinet/tcp.h>
#include "comdefs.h"
#include "tinyserver.h"
#include "error_404.h"

#define MIN_GET_SIZE 16

//Special names to try if url ends in '/'
char *defaulttext[4] = { "index.html", "index.htm", "index.shtml", "index.shtm" };

#define MAX_FILENAME_SIZE 255

void fn_error_404( int tid )

{
    int sizefile;
    sizefile = strlen( error_404 );
    sprintf( abuffer[tid], "HTTP/1.1 404 OK\n"
             "Server: tinyserver 1.0\r\n"
             "Content-Length: %d\r\n"
             "Connection: closed\r\n"
             "Content-Type: text/html; charset=iso-8859-1\r\n\r\n", sizefile );
    if ( send( ext_at_server[tid].ext_sock, abuffer[tid], strlen(abuffer[tid]), MSG_NOSIGNAL ) >= 0 )
        {
        send( ext_at_server[tid].ext_sock, error_404, strlen(error_404), MSG_NOSIGNAL );
        }
}

void *serverthread( void *ptid )

{
    BOOL fvalid;
    int action;
    int i;
    int ret;
    int tid;
    int len;
    int state;
    int oldstate;
    int sizefile;
    int ext;
    int last_fn_ch;
    int first_fn_ch;
    int ip;
    int fd;
    int val;
    char *host;
    char *setcookie;
    char *setcookienl;
    char *tail;
    char *psz;
    struct stat fstat;
    char filename[MAX_FILENAME_SIZE+1];
    char fauxfile[1024];


    prctl(PR_SET_PDEATHSIG, SIGHUP);
    tid = (int)ptid;
    for ( ;gnotexit; )
        {
        // Wait for something to do
        sem_wait(&(ext_at_server[tid].go_sem));
        if ( gnotexit )
            {
            val = 1;
            setsockopt( (int)ext_at_server[tid].ext_sock, IPPROTO_TCP, TCP_QUICKACK, (char *)&val, sizeof(int));
            ret = recv( (int)ext_at_server[tid].ext_sock, abuffer[tid], LOCAL_BUFFER_SIZE, MSG_NOSIGNAL);
            if ( ( ret == LOCAL_BUFFER_SIZE ) || ( ret < MIN_GET_SIZE ) )
                { // Don't mess with it.
                fprintf( stderr, "Bail #1 thread=%d ret=%d socket=%d\n", tid, ret, ext_at_server[tid].ext_sock );fflush(stderr);
                close( ext_at_server[tid].ext_sock ); // 9/28/2018
                printf( "goto#1:stopprocessing\n" );fflush(stdout);
                goto stopprocessing;
                }
            abuffer[tid][ret] = '\0';

            state = START_STATE;
            first_fn_ch = 0;
            last_fn_ch = 0;
            ext = 0;
            for ( i = 0; ( i < ret ) && ( state != ERROR_STATE ) && ( state != FIN_STATE ); i++ )
                {
                oldstate = state;
                state = webstatetable[state][abuffer[tid][i]];
                if ( ( oldstate == GET_STATE ) || ( oldstate == POST_STATE ) || ( oldstate == HEAD_STATE ) )
                    {
                    first_fn_ch = i;
                    }
                else if ( state == QUEST_STATE )
                    {
                    last_fn_ch = i;
                    if ( ( oldstate != RVAR_STATE ) && ( oldstate != LVAR_STATE ) && ( oldstate != SLASH_STATE ) && ( oldstate != FILENAME_STATE ) && ( oldstate != QUEST_STATE ) && ( oldstate != AMPER_STATE ) )
                        {
                        ext = webstatetable[oldstate][0];
                        }
                    if ( gname == 0 )
                        {
                        gvalue = abuffer[tid][i+3];
                        gname = abuffer[tid][i+1];
                        }
                    }
                }
            host = strstr( &(abuffer[tid][i]), "\nHost: " );
//BUGBUG
//Highly suspicious new code, trying to hack in a set cookie means from the router.
            setcookie = strstr( &(abuffer[tid][i]), "\nSet-Cookie: " );
            if ( setcookie )
                {
                setcookie++;
                setcookienl = strstr( setcookie, "\n" );
                if ( setcookienl )
                    {
                    *setcookienl = '\0';
                    setcookie = strdup( setcookie );
                    }
                else
                    {
                    setcookie = NULL;
                    }
                }
//End BUGBUG

            if ( state == ERROR_STATE )
                {
                fn_error_404( tid );
                goto stopprocessing;
                }
            if ( last_fn_ch == 0 )
                {
                last_fn_ch = i-1;
                }
            if ( ( state == FIN_STATE ) && ( oldstate != RVAR_STATE ) && ( oldstate != LVAR_STATE ) && ( oldstate != SLASH_STATE ) && ( oldstate != FILENAME_STATE ) && ( oldstate != QUEST_STATE ) && ( oldstate != AMPER_STATE ) )
                {
                ext = webstatetable[oldstate][0];
                }

            if ( (last_fn_ch-first_fn_ch+GURL_BASE_LEN) >= sizeof( filename ) )
                {
                printf( "Buffer overrun attempted\n" );fflush(stdout);
                goto stopprocessing;
                }
            strcpy( filename, gurl_base );
            strncpy( &(filename[GURL_BASE_LEN]), &(abuffer[tid][first_fn_ch]), last_fn_ch-first_fn_ch );
            filename[last_fn_ch-first_fn_ch+GURL_BASE_LEN] = '\0';
            DEBUGPRINTF( ( "filename=%s\n", filename ) );

            // This is not an optimal way to implement special files, this is in here for the exclock project.
            // Really need a better way to do this, later.
            if ( strcmp( filename, GURL_GENERATED_NAME ) == 0 )
                {
                sprintf( fauxfile, "function uds() {\nud6u(%d,%d,%d,%d,%d,%d,1);\n}\n", gd1, gd2, gd3, gd4, gd5, gd6 );
                sizefile = strlen( fauxfile );
                strcpy( abuffer[tid], "HTTP/1.1 200 OK\nServer: tinyserver 1.0\r\n"
                         "Cache-control: post-check=900,pre-check=3600\r\nContent-Length: " );
                ultoa( sizefile, 10, &(abuffer[tid][strlen(abuffer[tid])]) );
                strcat( abuffer[tid], "\r\nContent-Type: " );
                strcat( abuffer[tid], typetext[ext%(sizeof(typetext)/sizeof(typetext[0]))] );
                strcat( abuffer[tid], "\r\n\r\n" );
                if ( send( ext_at_server[tid].ext_sock, abuffer[tid], strlen(abuffer[tid]), MSG_NOSIGNAL ) < 0 )
                    {
                    printf( "goto#5:stopprocessing\n" );fflush(stdout);
                    goto stopprocessing;
                    }
                if ( send( ext_at_server[tid].ext_sock, fauxfile, sizefile, MSG_NOSIGNAL ) < 0 )
                    {
                    printf( "tid=%d trying send() again\n", tid );
                    if ( send( ext_at_server[tid].ext_sock, abuffer[tid], ret, MSG_NOSIGNAL ) < 0 )
                        {
            printf( "goto#7:stopprocessing\n" );fflush(stdout);
                        goto stopprocessing;
                        }
                    }
                send( ext_at_server[tid].ext_sock, abuffer[tid], 0, MSG_NOSIGNAL );
                }
            else
                {
                if ( stat( filename, &fstat ) < 0 )
                    {
                    fd = -1;
                    }
                else if ( S_ISDIR(fstat.st_mode) )
                    { // This is the trailing '/' in the URL case, try different defaults index.*...
                    len = strlen(filename);
                    if ( filename[len-1] != '/' )
                        {
                        if ( host )
                            {
                            host += 7;
                            tail = strstr( host, "\r" );
                            if ( tail == NULL )
                                {
                                printf( "tail is null\n" );fflush(stdout);
                                }
                            *tail = '\0';
                            strcpy( filename, host );
                            abuffer[tid][last_fn_ch] = '\0';
                            strcat( filename, &(abuffer[tid][first_fn_ch] ) );
                            strcpy( abuffer[tid], "HTTP/1.1 301\r\n"
                                "Server: tinyserver 1.0\r\n"
                                "Location: http://" );
                            strcat( abuffer[tid], filename );
                            strcat( abuffer[tid], "/\r\nConnection: close\r\n"
                                "Content-Type: text/html; charset=iso-8859-1\r\n\r\n" );
                            send( ext_at_server[tid].ext_sock, abuffer[tid], strlen(abuffer[tid]), MSG_NOSIGNAL );
                            // try this 04-07-2016
                            send( ext_at_server[tid].ext_sock, abuffer[tid], 0, MSG_NOSIGNAL );
                            }
                        printf( "goto#4:stopprocessing\n" );fflush(stdout);
                        goto stopprocessing;
                        }
                    else
                        {
                        psz = filename+strlen(filename);
                        ret = -1;
                        for ( i = 0; ( ( i < SZE(defaulttext) ) && ( ret < 0 ) ); i++ )
                            {
                            strcpy( psz, defaulttext[i] );
                            ret = stat( filename, &fstat );
                            }
                        if ( ret < 0 ) { goto stopprocessing; }
                        fd = open( filename, O_RDONLY, 0666 );
                        ext = TEXTHTML_STATE;
                        }
                    }
                else
                    { // just a normal file requested.
                    fd = open( filename, O_RDONLY, 0666 );
                    }
                if ( fd == -1 )
                    { // File not found
                    fn_error_404( tid );
                    goto stopprocessing;
                    }
                else
                    {
                    sizefile = fstat.st_size;
                    sprintf( abuffer[tid], "HTTP/1.1 200 OK\nHost: %s\r\nServer: tinyserver 1.0\r\n"
                         "Cache-control: post-check=900,pre-check=3600\r\nContent-Length: ", ghostpsz );
                    ultoa( sizefile, 10, &(abuffer[tid][strlen(abuffer[tid])]) );
//BUGBUG
//Highly suspicious new code, trying to hack in a set cookie means from the router.
                if ( setcookie )
                    {
                    strcat( abuffer[tid], "\r\n" );
                    strcat( abuffer[tid], setcookie );
                    free( setcookie );
                    setcookie = NULL;
                    }
//End BUGBUG
                    strcat( abuffer[tid], "\r\nContent-Type: " );
                    strcat( abuffer[tid], typetext[ext%(sizeof(typetext)/sizeof(typetext[0]))] );
                    strcat( abuffer[tid], "\r\n\r\n" );
                    if ( send( ext_at_server[tid].ext_sock, abuffer[tid], strlen(abuffer[tid]), MSG_NOSIGNAL ) < 0 )
                        {
                        printf( "goto#5:stopprocessing\n" );fflush(stdout);
                        goto stopprocessing;
                        }
                    do {
                        ret = read( fd, abuffer[tid], LOCAL_BUFFER_SIZE);
                        if ( ret < 0 )
                            {
                            send( ext_at_server[tid].ext_sock, abuffer[tid], 0, MSG_NOSIGNAL );
                            printf( "goto#6:stopprocessing\n" );fflush(stdout);
                            goto stopprocessing;
                            }
                        else
                            {
                            abuffer[tid][ret] = '\0'; // I don't think this is needed
                            if ( send( ext_at_server[tid].ext_sock, abuffer[tid], ret, MSG_NOSIGNAL ) < 0 )
                                {
                                printf( "tid=%d trying send() again\n", tid );
                                if ( send( ext_at_server[tid].ext_sock, abuffer[tid], ret, MSG_NOSIGNAL ) < 0 )
                                    {
                                    printf( "goto#7:stopprocessing\n" );fflush(stdout);
                                    goto stopprocessing;
                                    }
                                }
                            }
                    } while ( ret > 0 );
                    send( ext_at_server[tid].ext_sock, abuffer[tid], 0, MSG_NOSIGNAL ); //is this necessary? Slowing things down?
                    close(fd); //is this necessary? slowing things down?
                    }
                }

stopprocessing:
            close( ext_at_server[tid].ext_sock );
            ext_at_server[tid].state = READY_STATE;
            }
        }
//    sem_delete(&(ext_at_server[tid].go_sem));  //no delete, apparently.
    if ( tid < (NROUTERTHREADS-1) )
        {
        pthread_join( ext_at_server[tid+1].td, NULL );
        }
    printf( "Exit:Thread %d\n", tid );fflush(stdout);
}


