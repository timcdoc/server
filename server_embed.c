/*************************************************************************************/
/**                                                                                 **/
/** server_embed.c contains context sequence web server server                      **/
/** @timcdoc started ca 2014                                                        **/
/**                                                                                 **/
/*************************************************************************************/

#include <netinet/tcp.h>
#include "comdefs.h"

EXT_THREAD_GLOB ext_at_server[NROUTERTHREADS];
char abuffer[NROUTERTHREADS][LOCAL_BUFFER_SIZE+1];
void *main_thread( void *ptid );

int    volatile ext_sock;
int    volatile gnotexit;

pthread_mutex_t tcpip_mutex = PTHREAD_MUTEX_INITIALIZER;

struct sockaddr_in ext_sa;

unsigned char gurl_base[GURL_BASE_LEN] = GURL_BASE_NAME;

int server_embed( char *hostname, int iext_port )

{
    pthread_t atd;
    pthread_t notify_td;
    int reuseaddr;
    struct addrinfo hints, *pai;
    struct hostent     *ext_hp;

    gnotexit = 1;
    reuseaddr = 1;
    gname = 0;
    gvalue = 0;
    init_threads();

    memset( &hints, 0, sizeof( hints ) );
    hints.ai_family = PF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags |= AI_CANONNAME;

    if ( getaddrinfo( hostname, NULL, &hints, &pai ) != 0 )
        {
        fprintf( stderr, "Cannot get host %s's ip\n", hostname );
        exit( 1 );
        }
    else
        {
        while ( pai && ( pai->ai_family != AF_INET ) )
            {
            pai = pai->ai_next;
            }
        if ( pai == NULL )
            {
            fprintf( stderr, "Cannot get AF_INET of %s's ip\n", hostname );
            exit( 2 );
            }

        memcpy( &ext_sa, ((struct sockaddr_in *)pai->ai_addr), sizeof( struct sockaddr_in) );

        ext_sa.sin_family = PF_INET;
        ext_sa.sin_port = htons((u_short)(iext_port));

        if ( ( ext_sock = socket(PF_INET, SOCK_STREAM, 0) ) == INVALID_SOCKET )
            {
            fprintf( stderr, "Cannot get external socket %d\n", iext_port );
            exit( -1 );
            }
        else
            {
            setsockopt(ext_sock, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr));
            setsockopt( ext_sock, IPPROTO_TCP, TCP_NODELAY, &reuseaddr, sizeof(int));
            if ( bind( ext_sock, (struct sockaddr *)&ext_sa, sizeof( ext_sa ) ) == SOCKET_ERROR )
                {
                close( ext_sock );
                fprintf( stderr, "Cannot connect to external %s\n", hostname );
                exit( -1 );
                }
            }
        }
    listen( ext_sock, NROUTERTHREADS );

    pthread_create( &atd, NULL, main_thread, NULL );

    return( 0 );
}

void *main_thread( void *ptid )

{
    BOOL fnothandled;
    socklen_t in_len;
    int    this_socket;
    int   i;
    struct sockaddr_in cli_addr;

    in_len = sizeof(cli_addr);
    for ( ;gnotexit; )
        {
        DEBUGPRINTF( ("Before accept\n") );
        this_socket = accept(ext_sock, (struct sockaddr *)&cli_addr, &in_len);
        DEBUGPRINTF( ( "After accept this_socket=%d\n", this_socket ) );
        if ( this_socket == INVALID_SOCKET) 
            {
            fprintf(stderr, "Error waiting for new connection,try again in a second\n");
            usleep( 1000000 );
            }
        else
            {
            fnothandled = TRUE;
            do {
                for ( i = 0; i < NROUTERTHREADS; i++ )
                    {
                    // Find a state machine that isn't busy, and let that thread serve the request
                    if ( fnothandled && ( ext_at_server[i].state == READY_STATE ) )
                        {
                        ext_at_server[i].state = BUSY_STATE;
                        ext_at_server[i].ext_sock = this_socket;
                        ext_at_server[i].cli_addr = cli_addr;
                        sem_post(&(ext_at_server[i].go_sem));
                        fnothandled = FALSE;
                        }
                    sched_yield();
                    }
                } while ( fnothandled == TRUE );
            }
        }
    return( 0 );
}

