#!
#Assumption router has 32+32 threads
thisip=192.168.0.1
if ps -F -C webrouter | grep "webrouter 0 80 8000" >/dev/nul 2>&1
then
    ps -F -C webrouter | grep "webrouter 0 80 8000" 
    ps -F -C webrouter | grep "webrouter 0 80 8000" | awk "{ print \"kill -9 \" \$2 }" | bash
fi
if ps -F -C cldrouter | grep "cldrouter 0 80 8000" >/dev/nul 2>&1
then
    ps -F -C cldrouter | grep "cldrouter 0 80 8000" 
    ps -F -C cldrouter | grep "cldrouter 0 80 8000" | awk "{ print \"kill -9 \" \$2 }" | bash
fi
for (( port=13048; port<13102; port++ ))
do
    if ps -F -C server | grep "server $thisip $port" >/dev/nul 2>&1
    then
        ps -F -C server | grep "server $thisip $port" 
        ps -F -C server | grep "server $thisip $port" | awk "{ print \"kill -9 \" \$2 }" | bash
    fi
    rm -f /etc/router/80/GET,$thisip,$port
done
