// @timcdoc started ca 2014                                                        **/
#include "comdefs.h"

char *strrev( char *psz )

{
    int i;
    int len;
    char ch;
    len = strlen( psz );
    for ( i = 0; i < (len / 2); i++ )
        {
        ch = psz[i];
        psz[i] = psz[len-i-1];
        psz[len-i-1] = ch;
        }
}

void ultoa( long val, int base, char *psz )

{
    unsigned long i;
    char nary[16] = "0123456789abcdef";
    if ( ( base > 1) && ( base < 17 ) )
        {
        i = 0;
        do  {
            psz[i++] = nary[(val%base)];
            val /= base;
        } while ( val != 0 );
        psz[i] = '\0';
        strrev( psz );
        }
}
