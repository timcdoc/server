#!
#Assumption router has 32+32 threads
#run.sh /var/www/html
#ulimit -n 999999
#sysctl -w fs.file-max=1000000
thisip=192.168.0.1
if [ -z "$1" ]
then
    $0 /var/www/html
else
    while (true)
    do
        if ps -F -C router | grep "router 0 80 8000" >/dev/nul 2>&1
        then
            echo router already running.
        else
            echo starting router 0 80 8000
            router 0 80 8000 2>>../router/router.err.log &
        fi
    
        for (( port=13048; port<13080; port++ ))
        do
            if ps -F -C server | grep "server $thisip $port" >/dev/nul 2>&1
            then
                if ! [ -e /etc/router/80/GET,$thisip,$port ]; then
                    echo server $thisip $port already running
                    touch /etc/router/80/GET,$thisip,$port
                fi
            else
                echo rm -f /etc/router/80/GET,$thisip,$port
                rm -f /etc/router/80/GET,$thisip,$port
                touch /etc/router/80/GET,$thisip,$port
                echo starting server $thisip $port $1
                server $thisip $port $1 &
                sleep 2
                echo touch /etc/router/80/GET,$thisip,$port
            fi
        done
        sleep 10
    done
fi
