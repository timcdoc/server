/*************************************************************************************/
/**                                                                                 **/
/** init_threads.c contains thread and socket initializer for main.                 **/
/** @timcdoc started ca 2014                                                        **/
/**                                                                                 **/
/*************************************************************************************/

#include "comdefs.h"

void init_threads( void )

{
    int    i;

    for ( i = 0; i < NROUTERTHREADS; i++ )
        {
        sem_init(&(ext_at_server[i].go_sem), 0, 0);
        ext_at_server[i].state = READY_STATE;
        }

    for ( i = 0; i < NROUTERTHREADS; i++ )
        {
        pthread_create( &(ext_at_server[i].td), NULL, serverthread, (void *)(i ));
        }
}

