#!
set -f
if ! [ -e "/etc/router/81/CLD,$1,$2" ]; then
    touch "/etc/router/81/CLD,$1,$2"
    sleep 2
else
    rm -f "/etc/router/81/CLD,$1,$2"
    sleep 2
    touch "/etc/router/81/CLD,$1,$2"
fi
if ! [ -e "/etc/router/8001/CLD,$1,$3" ]; then
    touch "/etc/router/8001/CLD,$1,$3"
    sleep 2
else
    rm -f "/etc/router/8001/CLD,$1,$3"
    sleep 2
    touch "/etc/router/8001/CLD,$1,$3"
fi
